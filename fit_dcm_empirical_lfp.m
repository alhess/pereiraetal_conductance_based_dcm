function  DCM = fit_dcm_empirical_lfp(DCM,frange)
% ------------------------------------------------------------------------
% fit_dcm_empirical_lfp - Invert DCM on cross-spectral LFP data from an
%                         anaesthetized rodent.
%
% The data can be found at: https://www.fil.ion.ucl.ac.uk/spm/data/dcm_csd/
%
%--------------------------------------------------------------------------
% INPUT:
%       DCM     - struct: an SPM DCM struct.
%       frange  - 1x2 double: minimum and maximum frequency values. Both
%               define the frequency interval over which to fit the model.
%
%--------------------------------------------------------------------------
% OUTPUT: 
%       DCM     - struct: DCM with fitted posterior parameter estimates
%
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - July 2021
% Copyright 2021 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------
    
    DCM.M.Nmax   = 200;
    DCM.name     = 'Example-DCM-for-CSD';
    DCM.xY.Dfile = fullfile(fileparts(which(mfilename)),'external','dLFP_white_noise_r24_anaes','dLFP_white_noise_r24_anaes.mat');

    % -- Specify DCM struct -----------------------------------------------
    
    % Data
    DCM.xY.modality       = 'LFP';
    
    % Options
    DCM.options.trials    = 1;  % Just take the first trial
    DCM.options.analysis  = 'CSD';
    DCM.options.model     = 'CMM_NMDA';
    DCM.options.spatial   = 'LFP';
    DCM.options.Fdcm      = frange;
    DCM.options.Tdcm      = [1 10000];
    DCM.options.Nmodes    = 2;
    DCM.options.D         = 0;

    % Sources and connectivity
    DCM.Sname ={'A1', 'A2'};

    DCM.A{1} = [0 0; 
                1 0 ];

    DCM.A{2} = [0 1; 
                0 0 ];

    DCM.A{3} = zeros(length(DCM.Sname),length(DCM.Sname)); % Lateral connections?

    DCM.C    = 1';
    DCM.B    = [];
    DCM.xU.X = [];
    
    % -- Run it -----------------------------------------------------------
    disp('Inverting DCM model...')
    DCM = spm_dcm_csd(DCM);
end