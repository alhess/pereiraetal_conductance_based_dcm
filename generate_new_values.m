function [newValues, dist] = generate_new_values(nSim, nSigma, DCM, param, index, connectionType)
% -------------------------------------------------------------------------
% generate_new_values - Generates new parameter values from which to
%                       simulate data.
%
%--------------------------------------------------------------------------
% INPUT:
%       nSim     - double: Total number of simulations to conduct.
%
%       nSigma   - double: Maximal number of prior standard deviations (SD) 
%                  away from the prior mean. The new parameter values are
%                  defined on a linear interval between -nSigma*SD+mean and
%                  nSigma*SD+mean
%
%       DCM      - DCM struct. Necessary to extract the priors.
%
%       param    - char: Parameter class
%
%       index    - double: Parameter index within parameter class. With 
%                  param and index, this function has now the information 
%                  needed to select a precise parameter to change.
% 
% Optional:
%       connectionType    - double: optional unless param passed is 'A' or
%                          'AN' (extrinsic connectivity parameters). Then
%                          you need to indicate whether you would like to
%                          alter a forward or backward connection. 
%                          Forward: 1, Backward: 2.
%
%--------------------------------------------------------------------------
% OUTPUT: 
%       newValues    - 1 x nSim cell: new values from which to simulate
%                      data.
%
%       dist         - 1 x nSim double: number of SD away from prior mean.
%           
% -------------------------------------------------------------------------
% REFERENCE:
%           1. Pereira et al. (2021)
%                      Conductance-Based Dynamic Causal Modeling 
%      - A Mathematical Review of its Application to Cross-Power Spectral Densities -
%
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - June 2020
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------

    if ~exist('connectionType','var')
        connectionType = [];
    end
    
    if (~isequal(connectionType,1) && ~isequal(connectionType,2)) && (isequal(param, 'A') || isequal(param, 'AN'))
        error('You have supplied extrinsic connection parameters. Please indicate which connection type you would like to change. Forward: 1, Backward: 2.')
    end

    pE  = DCM.M.pE;     % Prior expectation
    pC	= DCM.M.pC;     % Prior covariance
    dist = linspace(-nSigma,nSigma,nSim);

    % Extract original value and create placeholder variable for new values
    %----------------------------------------------------------------------
    value = pE.(param);
    newValues = cell(1,nSim);
    
    % Fill placeholder with new values
    %----------------------------------------------------------------------
    % Extrinsic connections (A or AN) are a special case:
    if isequal(param, 'A') || isequal(param, 'AN')
        for ii=1:nSim
            new_value = value{connectionType};
            new_value(index) = value{connectionType}(index) + dist(ii)*sqrt(pC.(param){connectionType}(index));
            if connectionType == 1
                newValues{ii} = {new_value, cell2mat(pE.(param)(2))};
            elseif connectionType == 2
                newValues{ii} = {cell2mat(pE.(param)(1)),new_value};
            else
                error('Invalid type of extrinsic connection. Type 1 for forward and 2 for backward connections.')
            end
        end
    % Mg block parameter is also handled differently
    elseif isequal(param, 'Mg')  
        newValues = num2cell(0.01:0.01:0.12);
    % For all other parameters:
    else
        for ii=1:nSim
            new_value = value;
            new_value(index) = value(index) + dist(ii)*sqrt(pC.(param)(index));
            newValues{ii} = new_value;
        end
    end
    
end