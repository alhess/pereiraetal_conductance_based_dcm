function [f, Gu, S] = generate_csd(DCM)
% ------------------------------------------------------------------------
% generate_csd - Generate data (CSD) for single simulation.
%
%--------------------------------------------------------------------------
% INPUT:
%       DCM     - struct: an SPM DCM struct which contains all the necessary
%                         parameters needed for the simulations.
%
%--------------------------------------------------------------------------
% OUTPUT: 
%       f       - generated CSD
%
%       Gu      - Neuronal innovations in the frequency domain (see
%                 spm_csd_mtf.m)
%
%       S       - Transfer function (see spm_csd_mtf.m)
%
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - April 2020
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------

    M	= DCM.M;
    pE  = DCM.M.pE;
    U   = DCM.xU;

    % Create function handles
    %----------------------------------------------------------------------
    if isfield(M,'f'), M.f = spm_funcheck(M.f);  end
    if isfield(M,'g'), M.g = spm_funcheck(M.g);  end

    % Generate CSD
    %----------------------------------------------------------------------
    [~,f,Gu,S] = evalc('spm_csd_mtf(pE,M,U)');

end